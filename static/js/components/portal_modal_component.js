function load_portal_modal(){

Vue.component('portal-modal' ,{
  props:['logo'],
  data: function() {
    return {
      registering: false,
      email: "",
      password: "",
      confirm_password: "",
      first_name: "",
      last_name: "",
    }
  },
  methods: {
    toggle_registering: function() {
      this.registering = !this.registering;
    },

    sign_in: function() {
      if (!this.validate()) {
        return;
      }
      $.post(
        login_url,
        {
          email: this.email,
          password: this.password,
        },
        data => {
          switch(data.status) {
            case "fail":
              alert("invalid username or password");
              break;
            case "ok":
              this.$emit('signed-in', {
                email: this.email,
              });
              this.$emit('close');
              break;
          }
        }
      )
    },

    register: function() {
      if (!this.validate()) {
        return;
      }
      if (this.confirm_password !== this.password) {
        alert("passwords dont match!");
        return;
      }
      $.post(
        register_url,
        {
          email: this.email,
          password: this.password,
          first_name: this.first_name,
          last_name: this.last_name,
        },
        data => {
          switch(data.status) {
            case "exists":
              alert("email is already registered");
              break;
            case "fail":
              alert("creation failed for unknown reason");
              break;
            case "invalid":
              alert("invalid password")
              break;
            case "ok":
              this.$emit('signed-in', {
                email: this.email,
              });
              this.$emit('close');
              break;
          }
        }
      )
    },
    validate: function() {
      if (this.registering) {
        if (  this.first_name.length === 0 
            ||this.last_name.length === 0
            ||this.password.length === 0
            ||this.confirm_password.length === 0
            ||this.email.length === 0) {
          return false;
        }
        return true;
      } else {
        if (this.email.length === 0 || this.password.length === 0) {
          return false;
        }
        return true;
      }
    }
  },
  template: `
<transition name="modal">
  <div class="modal-mask">
    <div class="modal-wrapper">
      <div v-if="!registering" class="modal-container" style="width:30%; height: 40%;">
        <!-- header -->
        
        <div class="modal-header" style="height:20%; border-bottom:none;">       
          <span class="close"  @click="$emit('close')">&times;</span>
          <div class="modal-logo">
            <span class="logo" style="margin: 0 auto;">riis</span>
          </div>
        </div>

        <!-- signin form -->
        <div  class="modal-body">

          <label for="uname"><b>Email</b></label>
          <input class="" @keyup.enter="sign_in" v-model="email" type="text" placeholder="Enter Email" name="email" required>

          <label for="psw"><b>Password</b></label>
          <input @keyup.enter="sign_in" v-model="password" type="password" placeholder="Enter Password" name="psw" required>
          <div style="padding-top: 10px;">
            <button class= "or-ing" style="width:40%;" @click="sign_in">sign in</button>
            <button class= "or-ing"  style="width:40%;" @click="toggle_registering">register</button>
          </div>
          

        </div>
      </div>
      <div v-if="registering" class="modal-container" style="width:30%; height: 60%;">
        <!-- register form -->
        <div class="modal-header" style="height:20%; border-bottom:none;">       
          <span class="close"  @click="$emit('close')">&times;</span>
          <div class="modal-logo">
            <span class="logo" style="margin: 0 auto;">riis</span>
          </div>
        </div>

        <div class="modal-body">

          <label for="uname"><b>Email</b></label>
          <input @keyup.enter="register" v-model="email" type="text" placeholder="Enter Email" name="email" required>

          <label for="first"><b>First name</b></label>
          <input @keyup.enter="register" v-model="first_name" type="text" placeholder="Enter first name" name="email" required>

          <label for="last"><b>Last name</b></label>
          <input @keyup.enter="register" v-model="last_name" type="text" placeholder="Enter last name" name="email" required>

          <label for="psw"><b>Password</b></label>
          <input @keyup.enter="register" v-model="password" type="password" placeholder="Enter Password" name="psw" required>

          <label for="psw"><b>Confirm Password</b></label>
          <input @keyup.enter="register" v-model="confirm_password" type="password" placeholder="Enter Password" name="psw" required>
          <div style="padding-top: 10px;">
            <button class= "or-ing" style="width:40%;" @click="register">register</button>
            <button class= "or-ing" style="width:40%;" @click="toggle_registering">cancel</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</transition>
`
})
}