function load_profile_modal(){

Vue.component('profile-modal' ,{
  props:[],
  template: `
<transition name="modal">
  <div class="modal-mask">
    <div class="modal-wrapper">
      <div class="modal-container" style="width:40%;">
        <div class="modal-header" style="height: 50%;">
          <div class="logo">
            
          </div>
          <span class="close" @click="$emit('close')">&times;</span>
        </div>
       
      </div>
    </div>
  </div>
</transition>
`
})
}