function load_new_post_modal(){
Vue.component('new-post-modal' ,{
  props:[],
  data: function () {
	 return {
		zip_code: "",
		description: "",
		price: 0,
		title: "",
	 }
  },
  methods: {
		add: function() {
		  $("div.loader").show()
		  this.$emit('add-post', {
		    title: this.title,
		    zip_code: this.zip_code,
		    description: this.description,
		    price: this.price,
		    file: $("#file_input")[0].files[0],
		    completion_callback: () => {
		      $("div.loader").hide()
		      this.$emit('close')
		    }
		  })
		},
		inputChange: function(input) {
		// https://stackoverflow.com/questions/4459379/preview-an-image-before-it-is-uploaded/4459419#4459419
			if (input.target.files && input.target.files[0]) {
			  const reader = new FileReader();
			  reader.onload = function(e) {
				 $("#image_display").attr('src', e.target.result)
				 $("#image_display").show()
			  }
			  reader.readAsDataURL(input.target.files[0])
			}
		}
  },
  template: `
<transition name="modal">
  <div class="modal-mask">
	 <div class="modal-wrapper">
		<div class="modal-container container" style="width: 400px; height: 700px;">
		  <div class="modal-header" style="border-bottom: none;">
			 <span class="close" @click="$emit('close')">&times;</span>
		  </div>

			 <!-- Upload files -->
			 <div class="add-image">
				<input id="file_input" type="file" v-on:change="inputChange" accept="image/*">
				<div class="img-container">
				  <img id="image_display" style=" height: 300px;" src=""/>
				</div>
			 </div>
		  


		  <div class="post_input">
			 <div>
				<span>Title</span>
			 </div>
			 <input v-model="title" type="text">
		  </div>
		  <div class="post_input">
			 <div>
				<span>Description</span>
			 </div>
			 <input v-model="description" type="text">
		  </div>
		  <div class="post_input">
			 <div>
				<span>Zip code</span>
			 </div>
			 <input style="-webkit-appearance:none;-moz-appearance:textfield;" v-model="zip_code" type="number">
		  </div>
		  <div class="post_input">
			 <div>
				<span>Price</span>
			 </div>
			 <input v-model="price" type="number" step="0.01">
		  </div>

		  <!-- Submit -->
		  <div class="modal-footer" style="border-top: none; padding-top: 0;">
			 <div style="display:none;float:right;" class="loader"></div>
			 <button class="or-ing" style="border-radius: 8px;" @click="$emit('close')">Cancel</button>
			 <button class="or-ing" style="border-radius: 8px;" @click="add">Submit</button>
		  </div>
		</div>
	 </div>
  </div>
</transition>
`
})
}
