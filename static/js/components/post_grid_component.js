function load_post_grid(){

Vue.component('post-grid' ,{
	props:['posts'],
	methods: {
		bubble: function(data) {		
			this.$emit('view-post', {
				post: data.post
			})		
	    }
	},
	template: `
<div class="post_container" style="width: 100%; display:grid; grid-row-gap: 10px; grid-template-columns: 1fr 1fr 1fr;">
	<post v-on:view-post="bubble" v-for="post in posts" v-bind:post="post"></post>
</div>
`
})
}
