function load_post(){

Vue.component('post' , {
	data: function() {
		return {
			view_post: false
		}
	},
	methods: {
		view_post : function() {
			console.log("* * * * * ")
			this.$emit('view-post', {
		        post: this.post
		    })
		}

	},
	props:['post'],
	template: `
<div @click="view_post" class="post_cell lifted" style="text-align:center;" >
    <div class="post_content">
    	<div class="title">
    		<h3>{{post.title}}</h3>
    	</div>
			<div>
				<img v-bind:src="post.image" height="200px" width="200px" class="padded"/>
	    	</div>
    	<div>
    		<h5>{{post.post_city}}, {{post.post_state}}</h5>
			</div>
			<div class="description">
				<span>{{post.description}}</span>
			</div>
	  </div>
    <div class="post_feedback">
		<div class="interested">$ {{post.price}}</div>
    </div>

</div>
`
})
}
