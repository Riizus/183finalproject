function load_sidebar() {

Vue.component('sidebar' ,{
	props:[],
	data: function() {
		return {
			location: "",
			sort_by: "",
			lower_price: "",
			upper_price: "",
			by_me: false,
		}
	},
	methods: {
	    reset_sidebar: function() {
			this.location = '';
			this.sort_by = '';
			this.lower_price = '';
			this.upper_price = '';
			this.by_me = false;
	    },
		apply_filters: function() {
			this.$emit('apply-filters', {
				location: this.location,
				sort_by: this.sort_by,
				lower_price: this.lower_price,
				upper_price: this.upper_price,
				by_me: this.by_me,
			})
		},
	
	},
	template: `
<div class="sidebar">
	<div class="inner">
	  <span>Filters</span>
	  <button style="margin-left: 10px" class="or-ing" v-on:click="reset_sidebar">Reset</button>
	</div>
	<div class="filter_header">
		<div class="inner" style="">
	    	<span>Location</span>	  
	  	</div>
		<div style="background: #ffb30f;">
		  	<div class="inner">
		  		<input  class="or-ing" placeholder="Your location..." v-model="location" type="text" style="border: 1px solid #a58b15; background-color: #ddbb21; border-radius: 8px; margin-top: 5px;"/>
			</div>
		</div>
	</div>

	<div class="filter_header">
	  <div style="">
	    <span>Sort By</span>
	  </div>
	  <div style="">
	  	<ul style="padding-left: 0px; background-color:#ffb30f;">

		  	<li class="filter_list inner or-ing" v-on:click="sort_by = 'mr'">
		  		<div>
		  			<span>Most Recent</span>		
		  			<div class="check_container" >
		  				<label v-if="sort_by == 'mr'">
							<span class="checkmark">&#10004;</span>
						</label>
		  			</div>
		  		</div>
		  	</li>
		  	<li class="filter_list inner or-ing" v-on:click="sort_by = 'plh'">
		  		<div>
		  			<span>Price: low to high</span>		
		  			<div class="check_container" >
		  				<label v-if="sort_by == 'plh'">
							<span class="checkmark">&#10004;</span>
						</label>
		  			</div>
		  		</div>
		  	</li>
		  	<li class="filter_list inner or-ing" v-on:click="sort_by = 'phl'">
		  		<div>
		  			<span>Price: high to low</span>
		  			<div class="check_container">
		  				<label v-if="sort_by == 'phl'">
							<span class="checkmark">&#10004;</span>
						</label>
		  			</div>
		  		</div>
		  	</li>
		  </ul>	
	  </div>
	  
	</div>
	<div class="filter_header">
	  	<div class="inner">
	    	<span>Price</span>
	  	</div>
	  	<div style="background-color:#ffb30f;">
		 	<div class="price_range inner">
		  		<input class="or-ing" style="border: 1px solid #a58b15; margin-top: 5px; width: 45%; border-radius: 8px; background-color: #ddbb21;" v-model="lower_price" type="text"  /> 
		  		-
		  		<input class="or-ing" style="border: 1px solid #a58b15; margin-top: 5px; width: 45%; border-radius: 8px; background-color: #ddbb21;" v-model="upper_price" type="text"  />
		  </div>
		</div>
	</div>
	<div class="filter_header">
	  	<div style="background-color:#ffb30f;">
		  		<input class="or-ing" v-model="by_me" type="checkbox"  > By Me</input>
		</div>
	</div>


	<div class="inner">
		<button class="or-ing" v-on:click="apply_filters">Apply Filters</button>
	</div>
</div>
`
})
}
//<button class="change_location" v-on:click="">Change Location</button>