function view_post_modal(){

Vue.component('view-post-modal' ,{
  props:['post'],
  methods: {
    de : function() {
      console.log(this.post)
      this.$emit('close')
    }
  },
  template: `
<transition name="modal">
  <div class="modal-mask">
    <div class="modal-wrapper">
      <div class="modal-container" style="width:850px; height: 500px; border-radius: 8px;">
        <div class="modal-header" style="border-bottom: none;">
         <span class="close" @click="$emit('close')">&times;</span>
        </div>
        
        <div class="half">
          <div>
            <img v-bind:src="post.image" style="max-height: 400; height: auto; max-width:500px; width:auto; margin: auto; display: block;" class="padded"/>
          </div>
        </div>
        
        <div class="half" style="width: 350px; float: right; padding-right: 50px;">
      
       
          <h2>{{post.title}}</h2>
        

     
          <div style="">
           <div class="or-ing" style="border-radius: 8px;">
            <h4 style="height: 20px; padding-left: 5px;">Description</h4>
           </div>
           <div style="text-align: justify; text-indent: 30px; padding: 10px 0px;" >
            <span>{{post.description}}</span>
           </div>
          </div>
          <div style="">
           <div class="or-ing" style="border-radius: 8px;" >
            <h4 style="height: 20px; padding-left: 5px;">Zip code</h4>
           </div>
           <div style="padding: 10px 0px;">
            <span>{{post.zip_code}}</span>
           </div>
          </div>
          <div class="">
           <div class="or-ing" style="border-radius: 8px;">
            <h4 style="height: 20px; padding-left: 5px;">Price</h4>
           </div>
           <div style="padding: 10px 0px;">
            $<span>{{post.price}}</span>
           </div>
          </div>
          <div class="modal-footer" style="border-top: none; padding-top: 0;"> 
            <a style="color:black; float:right; padding:0.5em 1em; margin:0 0.5em 0.5em 0; display:inline-block; border-radius: 8px;" class="or-ing" v-bind:href="'mailto:' + post.user_email">Contact</a>
          </div>
          
      
      </div>
    </div>
    
  </div>
</transition>
`
})
}