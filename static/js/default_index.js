// This is the js for the default/index.html view.

var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    // Generic function to get element from list
    // List elements need id field.
    self.get_elem = function(list, id) {
      for (var i = 0; i < list.length; i++) {
        if (list[i].id == id){
          return list[i];
        }
      }
    };

    // Generic function to remove element from list
    // List elements need id field.
    self.remove_elem = function(list, id) {
      for (var i = 0; i < list.length; i++) {
        if (list[i].id == id){
          list.splice(i,1);
        }
      }
    };


    self.upload_image = function (post_data) {
        let file = post_data.file;

        if (file) {
            var req = new XMLHttpRequest();
            req.addEventListener("load", () => {
              self.upload_complete(JSON.parse(req.response).url, post_data)
            });
            req.open("POST", "https://api.cloudinary.com/v1_1/drqsikpku/image/upload", true);
            let formData = new FormData();
            formData.append("upload_preset", "bjjk4oto")
            formData.append("file", file)
            req.send(formData);
        }

    };

    self.upload_complete = function(get_url, post_data) {
        console.log("image uploaded")
        // Hides the uploader div.
        $.post(create_post_url,
            {
                image_url: get_url,
                description: post_data.description,
                title: post_data.title,
                zip_code: post_data.zip_code,
                price: post_data.price,
            },
            function(data) {
                self.vue.posts.unshift(data.post)

                post_data.completion_callback();
                console.log(data)
                // TODO: do something with this
                console.log('The post was uploaded; it is now available');
            }
        );
    };

    self.get_more_posts = function() {

        self.vue.has_more = false;

        let endpoint;
        if (self.vue.page === 'home') {
            endpoint = get_posts_url;
        } else if (self.vue.page === 'search') {
            endpoint = search_url;
        }
        $.getJSON(
            endpoint,
            {
                query: self.vue.query,
                start: self.vue.posts.length,
                location: self.vue.filter.location,
                sort_by: self.vue.filter.sort_by,
                lower_price: self.vue.filter.lower_price,
                upper_price: self.vue.filter.upper_price,
                by_me: self.vue.filter.by_me,
            },
            data => {
                self.vue.posts = self.vue.posts.concat(data.posts);
                self.vue.has_more = data.has_more;
            }
        )
    }

    self.goto = function(page) {
        self.vue.page = page;
        if (self.vue.page === 'search' || self.vue.page === 'home') {
            self.vue.posts = [];
            self.get_more_posts();
        }
    }

    self.signed_in = function(data) {
        self.vue.logged_in = true;
        self.vue.user_email = data.email;
    }

    self.sign_out = function(data) {
        if (confirm('are you sure you want to log out?')) {
            $.post(logout_url, data => {
                self.vue.logged_in = false;
                self.vue.user_email = "";
            })
        }
    }

    self.search = function() {
        // Searches for Query, returns users and posts
        $.getJSON(search_url, {query: self.vue.query},
            function(data) {
                self.vue.posts = data.posts;
            }
        );
    };

    self.apply_filters = function(data) {
        
        self.vue.posts = [];
        self.vue.filter = {
            ...data
		};

        self.get_more_posts();
    };

    self.view_post = function(data) {
    	console.log(data.post)
        self.vue.viewed_post = data.post
    	self.vue.showViewPost = true;
    }


    load_components();
    console.log("load")


    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
          logged_in: false,
          user_email: "",
          is_uploading: false,
          query: '',
          page: 'home',
          posts: [],
          filter: {},
          showNewPost: false,
          showPortal: false,
          showViewPost: false,
          has_more: false,
          viewed_post: {}
        },
        methods: {
          open_uploader: self.open_uploader,
          close_uploader: self.close_uploader,
          upload_image: self.upload_image,
          get_posts: self.get_posts,
          get_more_posts: self.get_more_posts,
          goto: self.goto,
          signed_in: self.signed_in,
          sign_out: self.sign_out,
          apply_filters: self.apply_filters,
          view_post: self.view_post,
          remove_left: function (id) {
            self.remove_elem(left_posts,id);
          },
          remove_right: function (id) {
            self.remove_elem(right_posts,id);
          },
          dbug: function () {
            console.log("* * * ")
          }
        }
    });
    login_hack_div = $("#login_hack_div")[0]
    if (login_hack_div) {
        self.vue.logged_in = true;
        self.vue.user_email = login_hack_div.innerText;
    }
    $("#vue-div").show()
    self.get_more_posts();


    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});
