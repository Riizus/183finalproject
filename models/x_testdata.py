from gluon.storage import Storage
import random
from datetime import datetime
import badwords

# set this to clear the tables
RESET = False
# toggle testdata generation
ENABLE_TEST_DATA_GENERATION = True

def get_n_words(words, n):
    result = ""
    for i in range(0,n):
        choice = random.choice(words)
        while badwords.IS_BAD()(choice)[1]:
            choice = random.choice(words)
        result += " " + choice
    return result


if RESET:
    print("resetting tables")
    for table in db.tables:
        # Make sure to cascade, or this will fail
        # for tables that have FK references.
        db[table].truncate("CASCADE")
    db.commit()

if ENABLE_TEST_DATA_GENERATION:
    # if there are no users
    if db(db.auth_user.id > 0).count() == 0:
        alphabet = 'abcdefghijklmnopqrstuvwxyz'
        for letter in alphabet:
            user = auth.register_bare(first_name=letter, last_name=letter,
                                    email='%s@%s.com' % (letter, letter),
                                    username=letter, password='qwer')

    zip_codes = set([
        "35801",
        "99501",
        "85001",
        "72201",
        "94203",
        "80201",
        "06101",
        "19901",
        "20001",
        "32501",
        "33124",
        "30301",
        "96801",
        "60601",
        "62701",
        "46201",
        "52801",
    ])

    # if there are no posts
    if db(db.posts.id > 0).count() == 0:
        WORDS = open("/usr/share/dict/words").read().splitlines()
        print("adding posts")
        alphabet = 'abcdefghijklmnopqrstuvwxyz'
        for letter in alphabet:
            for i in range(0,5):
                user = db((db.auth_user.first_name == letter) & (db.auth_user.last_name == letter)).select().first()
                db.posts.insert(
                    image="https://i.imgur.com/awrJ4Ne.jpg",
                    description=get_n_words(WORDS, random.randint(10, 60)),
                    title=get_n_words(WORDS, random.randint(1, 3)),
                    user_id=user.id,
                    user_email=user.email,
                    zip_code=random.sample(zip_codes, 1)[0],
                    price=random.randint(100, 50000)/100.0,
                    created_on=datetime(2018, 5, random.randint(1, 31), random.randint(0, 23)),
                )
