# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.

import datetime
import csv
import os

zipmap = dict()

with open(os.path.dirname(__file__) + '/zipcodes.csv') as f:
    reader = csv.reader(f)
    for row in reader:
        zipmap[row[0]] = row[2:4]

def get_user_email():
    return auth.user.email if auth.user is not None else None

def get_user_id():
    return auth.user.id if auth.user is not None else None

def get_state(row):
    location = ["Invalid", "NO"]
    if row.zip_code in zipmap:
        location = zipmap[row.zip_code]
    return location[1]

def get_city(row):
    location = ["Invalid", "NO"]
    if row.zip_code in zipmap:
        location = zipmap[row.zip_code]
    return location[0].title()


db.define_table('posts',
                Field('image'),
                Field('description', 'text'),
                Field('user_email', default=get_user_email()),
                Field('created_on', 'datetime', default=datetime.datetime.utcnow()),
                Field('zip_code', default="00000"),
                Field('title', default="DEFAULT POST TITLE"),
                Field('user_id', db.auth_user, default=get_user_id()),
                Field('price', 'double', default=1.99),
                Field('post_state', compute=get_state),
                Field('post_city', compute=get_city),
                )


# after defining tables, uncomment below to enable auditing
auth.enable_record_versioning(db)
