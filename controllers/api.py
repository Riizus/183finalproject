# Here go your api methods.
import csv

DEFAULT_RESULT_COUNT = 20

def signin():
    if not request.vars.password or not request.vars.email:
        return HTTP(500)
    if not auth.login_bare(request.vars.email, request.vars.password):
        return response.json(dict(status="fail"))
    return response.json(dict(status="ok"))

def register():
    if not (request.vars.email and request.vars.password and request.vars.first_name and request.vars.last_name):
        return HTTP(500)

    existing = db(db.auth_user.email == request.vars.email).count()
    if existing > 0:
        return response.json(dict(status="exists"))

    ret = auth.register_bare(
        first_name = request.vars.first_name,
        last_name = request.vars.last_name,
        email = request.vars.email,
        password = request.vars.password
    )
    if ret and len(ret.password) == 0:
        ret.delete_record()
        return response.json(dict(status="invalid"))
    
    if not ret:
        return response.json(dict(status="fail"))

    created = auth.login_bare(request.vars.email, request.vars.password)
    if not created:
        return response.json(dict(status="fail"))

    return response.json(dict(status="ok"))

def logout():
    if auth.user:
        auth.logout()

@auth.requires_login()
def create_post():

    post_id = db.posts.insert(
        image=request.vars.image_url, 
        description=request.vars.description,
        title=request.vars.title,
        zip_code=request.vars.zip_code,
        price=request.vars.price)
    return response.json(dict(post=db.posts[post_id]))

# expects post ID as id
@auth.requires_login()
def remove_post():
    post_id = request.vars.id
    if post_id is None:
        return HTTP(500)

    found_post = db(db.posts.id == post_id).select().first()

    if found_post.user_id != auth.user.id:
        return HTTP(500)

    found_post.delete_record()

# generate a query from the filters, if they are set
def filtered_query(location, lower_price, upper_price, by_me):
    queries = []
    if location:
        queries.append(((db.posts.post_city.contains(location)) | 
                  (db.posts.post_state.contains(location))))
    if lower_price:
        queries.append((db.posts.price >= lower_price))
    if upper_price:
        queries.append((db.posts.price <= upper_price))

    if auth.user and by_me == 'true':
        queries.append((db.posts.user_id == auth.user.id))

    if len(queries) > 0:
        return reduce(lambda a,b: (a&b), queries)
    else:
        return None

def sortby_clause(sortby):
    if sortby == 'mr':
        return ~db.posts.created_on
    elif sortby == 'plh':
        return db.posts.price 
    elif sortby == 'phl':
        return ~db.posts.price
    return None

# accepts
# {
#    start: num 
#    end:   num
#    location: string
#    lower_price: string
#    upper_price: string
#    sort_by: string(mr, plh, phl)
# }
# return {
#   posts: []
#   hasmore: bool
# }
def get_posts():
    start_idx = int(request.vars.start) if request.vars.start is not None else 0
    end_idx = int(request.vars.end) if request.vars.end is not None else start_idx + DEFAULT_RESULT_COUNT

    query = (db.posts.id > 0)

    fquery = filtered_query(
        request.vars.location, 
        request.vars.lower_price, 
        request.vars.upper_price,
        request.vars.by_me)
    if fquery:
        query &= fquery

    print(query)
    found_posts = None
    if request.vars.sort_by:
        found_posts = db(query).select(
            db.posts.ALL, 
            orderby=sortby_clause(request.vars.sort_by),
            limitby=(start_idx, end_idx))
    else: 
        found_posts = db(query).select(
            db.posts.ALL, 
            limitby=(start_idx, end_idx))
    amount = db(query).count()

    return response.json(
        dict(
            posts=found_posts,
            has_more=amount > end_idx))


# accepts
# {
#    query: string
#    start: num 
#    end:   num
#    location: string
#    lower_price: string
#    upper_price: string
#    sort_by: string(mr, plh, phl)
# }
# return {
#   posts: []
#   hasmore: bool
# }
#@auth.requires_signature()
def search():
    search_string = request.vars.query
    start_idx = int(request.vars.start) if request.vars.start is not None else 0
    end_idx = int(request.vars.end) if request.vars.end is not None else start_idx + DEFAULT_RESULT_COUNT

    if search_string is None:
        return HTTP(500)
    
    query = (db.posts.title.contains(search_string)) | (db.posts.description.contains(search_string))

    fquery = filtered_query(
        request.vars.location, 
        request.vars.lower_price, 
        request.vars.upper_price,
        request.vars.by_me)
    if fquery:
        query &= fquery

    found_posts = None
    if request.vars.sort_by:
        found_posts = db(query).select(
            db.posts.ALL, 
            orderby=sortby_clause(request.vars.sort_by),
            limitby=(start_idx, end_idx))
    else: 
        found_posts = db(query).select(
            db.posts.ALL, 
            limitby=(start_idx, end_idx))
    amount = db(query).count()

    return response.json(
        dict(
            posts=found_posts, 
            has_more=amount > end_idx))
